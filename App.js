import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Register from './pages/register';
import Login from './pages/login';
import Dashboard from './pages/dashboard';

export default function App() {
  return (
      <Dashboard/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
