import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { Button, ScrollView, StyleSheet, Text, TextInput, View } from 'react-native';

export default function login() {
  return (
    <View style = {styles.container}>
      <ScrollView>
        <View >
          <Text style={styles.header}>Welcome</Text>
          <Text style={styles.subheader}>Sign in to continue</Text>
        </View>
        <View style = {styles.formSignUp}>
            <Text style={styles.label}>Email</Text>
            <TextInput
                style={styles.input}
                placeholder= 'email@email.com'
            />

            <Text style={styles.label}>Password</Text>
            <TextInput
                style={styles.input}
                placeholder= '*************'
            />

            <Text style={{textAlign: 'right', paddingTop:10}}>Forgot Password? </Text>
            <View style={{marginBottom:20}}/>
            <Button title="Sign In" color="#f87217"></Button>
            <Text style={{textAlign: 'center', padding:10}}>- OR -</Text>
            <View style={styles.sso}>
                <View style={styles.ssoBtn}><Text>facebook</Text></View>
                <View style={styles.ssoBtn}><Text>Google</Text></View>
            </View>
        </View>
        
       
      </ScrollView>

      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
    container: {
      justifyContent: 'center',
      flex: 1,
      marginTop: 20,
      padding: 20,
      backgroundColor: '#f1f1f1'
    },
    input: {
      borderBottomColor: 'gray', 
      borderBottomWidth: 1,
      backgroundColor: 'white',
      borderRadius: 4,
      width: '100%',
    },
    label: {
      marginTop:20
    },
    formSignUp: {
      alignSelf: 'center',
      backgroundColor: 'white',
      width: 300,
      borderWidth: 1,
      borderColor: 'gray',
      borderRadius: 10,
      padding: 10,
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.5,
      shadowRadius: 2,
      elevation: 2,
    },
    header: {
      fontWeight: 'bold',
      fontSize: 40,
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.5,
      shadowRadius: 2,
      elevation: 2,
      height: 45
    },
    subheader: {
      marginBottom: 20,
      paddingTop: 0,
      color: 'gray'
    },
    sso: {
        flexDirection: 'row'
    },
    ssoBtn: {
        flex:1,
        borderColor: 'gray',
        borderRadius: 3,
        borderWidth: 1,
        marginHorizontal: 10,
        padding:10,
        alignItems: 'center',
    }
  });
